'use strict';

angular.module('foodMeApp', [
          'ngRoute',
          'ngResource',
          'CustomerComponent',
          'RestaurantComponent',
          'MenuComponent',
          'CheckoutComponent',
          'ThankYouComponent',
          'DeliverToDirective',
          'RatingDirective',
          'CheckboxListDirective',
          'CustomerService',
          'RestaurantService',
          'CartService',
          'LocalStorageValue',
          'AlertValue'
])
.config(['$locationProvider', '$routeProvider' , function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
      $routeProvider
      .when('/', {
          controller: 'RestaurantsCtrl',
          templateUrl: 'app/views/restaurants.html'
      })
      .when('/menu/:restaurantId', {
        controller: 'MenuCtrl',
        templateUrl: 'app/views/menu.html'
      })
      .when('/checkout', {
        controller: 'CheckoutCtrl',
        templateUrl: 'app/views/checkout.html'
      })
      .when('/thank-you', {
        controller: 'ThankYouCtrl',
        templateUrl: 'app/views/thankyou.html'
      })
      .when('/customer', {
        controller: 'CustomerCtrl',
        templateUrl: 'app/views/customer.html'
      })
      .when('/who-we-are', {
        templateUrl: 'app/views/who-we-are.html'
      })
      .when('/how-it-works', {
        templateUrl: 'app/views/how-it-works.html'
      })
     .when('/help', {
        templateUrl: 'app/views/help.html'
      })
      .otherwise('/');
}]);
