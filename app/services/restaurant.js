'use strict';

angular.module('RestaurantService', []).factory('restaurant', ['$resource', function($resource) {

        return $resource('app/restaurants/restaurants.json/:id', {id: '@id'});
}]);
