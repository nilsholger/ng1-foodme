'use strict';
angular.module('DeliverToDirective', []).directive('deliverTo', function() {
      return {
        restrict: 'E',
        templateUrl: 'app/directives/deliverto.html',
        scope: {},
        controller: function DeliverToCtrl($scope, customer) {
          $scope.customer = customer;
        }
      };
});
