'use strict';

foodMeApp.controller('NavbarCtrl', ['$scope', '$location', function($scope, $location) {
                $scope.routeIs = function(routeName) {
                  return $location.path() === routeName;
                };
}]);
