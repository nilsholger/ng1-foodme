'use strict';

angular.module('CheckoutComponent', []).controller('CheckoutCtrl', ['$scope', 'cart', 'customer',
                '$location', function($scope, cart, customer, $location) {
                 $scope.cart = cart;
                 $scope.restaurantId = cart.restaurant.id;
                 $scope.customer = customer;
                 $scope.submitting = false;

                 $scope.purchase = function() {
                   if ($scope.submitting) { return; }

                   $scope.submitting = true;
                   cart.reset();
                   $location.url('/thank-you');
                 };
}]);
