'use strict';

angular.module('MenuComponent', []).controller('MenuCtrl', ['$scope', '$routeParams', 'restaurant', 'cart',
  function($scope, $routeParams, restaurant, cart) {
    var rs = restaurant.query().$promise.then(function(data){
    for (var i = 0; i < data.length; i++) {
      if ($routeParams.restaurantId === data[i].id) {
        $scope.restaurant = data[i];
      }
    }
  });
  $scope.cart = cart;
}]);
