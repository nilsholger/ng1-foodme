'use strict';

angular.module('CustomerComponent', []).controller('CustomerCtrl',
        ['$scope', 'customer', '$location', function($scope, customer, $location) {

          $scope.customerName = customer.name;
          $scope.customerAddress = customer.address;

        $scope.findRestaurants = function(customerName, customerAddress) {
              customer.name = customerName;
              customer.address = customerAddress;
                $location.url('/');
        };
}]);
